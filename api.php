<?php
// dummy constants
// TODO: replace paths as needed
$DIRPATH = "/Library/WebServer/Documents/tindahansync/temp/";
$URIPATH = "http://192.168.0.198/tindahansync/";
$YOURDEVICEID = "358490041793853";

// TODO: this should be on a config and should be different for test and live environment
// 		 this is to protect the API from unauthorized access
$key = "NtNKzRwsgs2qPJFg8SPvE8kG";

// dummy api
/*
	NOTES: 
	
	data->item->devices are unique device ids
	
	ACTIONS
	-------
		getSyncList - list all purchased books
		syncToDevice - triggers the conversion of the ebook from raw to DRMed epub
		getSyncStatus - checks if the preparation is complete, once STATUS = 100 it is ready for download
			
	getSyncList
	-----------
		PARAMS:
			paypil_no - members account no
			device_id - unique id of the device		
		RETURNS:
			array consisting of
				* bookId
				* title
				* author
				* publisher
				* imgUrl - cover image of the book
				* itemType - either book or magazine
				* devices - array of unique device id which the book is already linked to
				* max - maximum allowable syncing
		ERROR RETURN:
			array consisting of
				* error - the error message

	syncToDevice
	------------
		PARAMS:
			paypil_no - members account no
			device_id - unique id of the device		
			book_id - target book to sync
		RETURNS:
			array consisting of
				* status - with value TRUE, means that the preparation has already started				
		ERROR RETURN:
			array consisting of
				* error - the error message

	getSyncStatus
	-------------
		PARAMS:				
			paypil_no - members account no
			device_id - unique id of the device		
			book_id - target book to sync
		RETURNS:
			array consisting of
				* bookId
				* url - where to download, empty if not ready
				* status - percentage of conversion, 100 if ready for download
		ERROR RETURN:
			array consisting of
				* error - the error message
				
	ADDITIONAL NOTES:
	-----------------
	
	Since this is a dummy the data won't get updated, these are provided to test the display depending on the scenario
	
	BOOK 1 is synced to 5 devices, so it can no longer be synced, 
		unless the device id is listed in devices and the book is not yet synced (in case of re-installation)
	BOOK 2 is already synced to your device, replace the "REPLACETHIS" with your device id
	BOOK 3 is already synced to 2 devices, so it can be synced to the device not listed
	BOOK 4 is not yet synced to any device
	
	STATUS = percentage of EPUB conversion, 100+ means ready for download, the url will no longer be empty
	
	TO RE-TEST syncToDevice and getSyncStatus, just delete the generated files at $DIRPATH
	
	BOOK is reflowable, MAGAZINE is fixed-format, Advanced sample of SkyEpub shows how to display that
*/
$json = '
	{"data":[
		{"item":
			{
			"bookId":1,
			"title":"Moby Dick",
			"author":"Herman Melville",
			"publisher":"Great American Novels",
			"imgUrl":"' . $URIPATH. 'covers/moby-dick.jpg",
			"itemType":"book",
			"devices": ["123451", "123452", "123453", "123454", "' . $YOURDEVICEID . '"],
			"max":5
			}
		},
		{"item":
			{
			"bookId":2,
			"title":"The Voyage of Life",
			"author":"Thomas Cole",
			"publisher":"Munson Williams Proctor Inst",
			"imgUrl":"' . $URIPATH. 'covers/voyage-of-life.jpg",
			"itemType":"book",
			"devices": ["' . $YOURDEVICEID . '"],
			"max":5
			}
		},
		{"item":
			{
			"bookId":3,
			"title":"The Wasteland",
			"author":"Susan Kim, Laurence Klavan",
			"publisher":"HarperTeen",
			"imgUrl":"' . $URIPATH. 'covers/wasteland.jpg",
			"itemType":"book",
			"devices": ["123451", "123452"],
			"max":5
			}
		},
		{"item":
			{
			"bookId":4,
			"title":"Sample from Adobe In-Design",
			"author":"kendoka",
			"publisher":"Tindahan",
			"imgUrl":"' . $URIPATH. 'covers/kendo-in-design-sample.jpg",
			"itemType":"magazine",
			"devices": [],
			"max":5
			}
		}
	]}';
$books = json_decode($json, true);

function getSyncList($paypil_no, $device_id) {
	global $books;
	
	if ($paypil_no != "999-999-999")
		$res = buildError("Invalid paypil no");
	else if ($device_id == "")
		$res = buildError("Invalid device id");
	else
		$res = json_encode($books);
		
	return $res;
}

function syncToDevice($paypil_no, $device_id, $book_id) {
	global $DIRPATH;
	global $YOURDEVICEID;

	if ($book_id < 1 || $book_id >  4)
		$res = buildError("Invalid book id");
	else if ($book_id == 1 && $device_id != $YOURDEVICEID)
		$res = buildError("Reached the maximum allowable syncing for this book");
	else if ($paypil_no != "999-999-999")
		$res = buildError("Invalid paypil no");
	else if ($device_id == "")
		$res = buildError("Invalid device id");
	else {
		// simulate preparation
		$fn = $DIRPATH . $paypil_no . "-" . $device_id . "-" . $book_id;
		if (file_exists($fn))
			$res = buildError("Preparation already started");
		else {
			$handle = fopen($fn, "w");
			fwrite($handle, 0);
			fclose($handle);

			$res = '{"data":[{"item":{"status":true}}]}';	
		}
	}
	
	return $res;
}

function getSyncStatus($paypil_no, $device_id, $book_id) {
	global $DIRPATH;
	global $URIPATH;
	global $YOURDEVICEID;
	
	if ($book_id < 1 || $book_id >  4)
		$res = buildError("Invalid book id");
	else if ($book_id == 1 && $device_id != $YOURDEVICEID)
		$res = buildError("Reached the maximum allowable syncing for this book");
	else if ($paypil_no != "999-999-999")
		$res = buildError("Invalid paypil no");
	else if ($device_id == "")
		$res = buildError("Invalid device id");
	else {
		// simulate syncing
		$fn = $DIRPATH . $paypil_no . "-" . $device_id . "-" . $book_id;
	
		$percentage = 0;
		
		if (!file_exists($fn))
			$res = buildError("Target book is not yet synced");
		else {
			$handle = fopen($fn, "r");
			$percentage = intval(fread($handle, 1024));
			fclose($handle);
			
			$percentage += 20;
			if ($percentage > 100)
				$percentage = 100;
			$handle = fopen($fn, "w");
			fwrite($handle, $percentage);
			fclose($handle);

			if ($percentage == 100) {
				$dummy_books = array(
					"moby-dick.epub", 
					"voyage-of-life.epub", 
					"wasteland.epub", 
					"kendo-in-design-sample.epub"
					);
				$url = $URIPATH . "books/" . $dummy_books[$book_id-1];
			} else		
				$url = "";
			
			$res = '{"data":[{"item":{"bookId":' . $book_id . ',"url":"' . $url . '","status":' . $percentage . '}}]}';	
		}
	}		
	
	return $res;
}

function buildError($message) {
	return '{"data":[{"item":{"error":"' . $message . '"}}]}';
}

// handler
$possible_url = array("getSyncList", "syncToDevice", "getSyncStatus");

if (!isset($_GET["key"]))
	$value = buildError("Missing API key");
else if ($_GET["key"] != $key)
	$value = buildError("Invalid API key");
else if (isset($_GET["action"]) && in_array($_GET["action"], $possible_url))
	{
	switch ($_GET["action"])
		{
		case "getSyncList":
			if (isset($_GET["paypil_no"])
					&& isset($_GET["device_id"]))
				$value = getSyncList($_GET["paypil_no"], $_GET["device_id"]);
			else
				$value = buildError("Missing list argument (paypil_no, device_id)");
			break;
		case "syncToDevice":
			if (isset($_GET["paypil_no"])
					&& isset($_GET["device_id"])
					&& isset($_GET["book_id"]))
				$value = syncToDevice($_GET["paypil_no"], $_GET["device_id"], $_GET["book_id"]);
			else
				$value = buildError("Missing link argument (paypil_no, device_id, book_id)");
			break;
		case "getSyncStatus":
			if (isset($_GET["paypil_no"])
					&& isset($_GET["device_id"])
					&& isset($_GET["book_id"]))				
				$value = getSyncStatus($_GET["paypil_no"], $_GET["device_id"], $_GET["book_id"]);
			else
				$value = buildError("Missing sync argument (paypil_no, device_id, book_id)");
			break;
		}
	}
else
	$value = buildError("Invalid action");

// output
@header("Content-Type: application/json");
exit(stripcslashes($value));
?>
